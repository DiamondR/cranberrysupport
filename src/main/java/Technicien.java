

import java.util.ArrayList;

public class Technicien extends Utilisateur {

    //Listes importantes
    private ArrayList<Requete> ListeRequetesTech;
    private ArrayList<Requete> ListeRequetesFinies;
    private ArrayList<Requete> ListeRequetesEnCours;
    private ArrayList<Requete> ListeRequetesPerso;

    //Petit constructeur
    Technicien(String nom, String mdp, String role) {
        super(nom, mdp, role);
    }

    //Constructeur complet
    Technicien(String prenom, String nom, String nomUtilisateur, String mdp, String role) {
        super(prenom, nom, nomUtilisateur, mdp, role);
        ListeRequetesTech = new ArrayList<Requete>();
        ListeRequetesEnCours = new ArrayList<Requete>();
        ListeRequetesFinies = new ArrayList<Requete>();
        ListeRequetesPerso = new ArrayList<Requete>();
    }

    @Override
    public String getRole() {
        return "technicien";
    }

    @Override
    public ArrayList getListeRequetes() {
        return ListeRequetesTech;
    }

    //Actions lorsqu'il se fait assigner une requete
    public void ajouterRequeteAssignee(Requete assignee) {
        ListeRequetesEnCours.add(assignee);
        ListeRequetesTech.add(assignee);
    }

    //Action sur les liste lorsqu'une requete est finalisée
    public void ajoutListRequetesFinies(Requete finie) {
        ListeRequetesFinies.add(finie);
        ListeRequetesEnCours.remove(finie);
    }

    //Ajoute une requete (qu'il a lui-même créée)
    @Override
    public void ajoutRequete(Requete nouvelle) {
        ListeRequetesTech.add(nouvelle);
        ListeRequetesPerso.add(nouvelle);
    }

    @Override
    public ArrayList<Requete> getListPerso() {
        return ListeRequetesPerso;
    }

    //Retourne la liste des requetes de ce statut
    @Override
    public ArrayList<Requete> getListStatut(Requete.Statut statut) {
        ArrayList<Requete> r = new ArrayList<Requete>();
        for (int i = 0; i < ListeRequetesTech.size(); i++) {
            if (ListeRequetesTech.get(i).getStatut().equals(statut)) {
                r.add(ListeRequetesTech.get(i));
            }

        }
        return r;
    }

    //Retourne une string contenant pour ce technicien le nombre
    //de requetes en fonction de leur statut
    public String getRequeteParStatut() {
        String ouvert = "Statut ouvert: ";
        int ouv = 0;
        String enTraitement = "Statut En traitement: ";
        int tr = 0;
        String succes = "Statut succès: ";
        int su = 0;
        String abandon = "Statut abandonnée: ";
        int ab = 0;
        String parStatut = "";
        for (int i = 0; i < ListeRequetesTech.size(); i++) {
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.ouvert)) {
                ouv++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.enTraitement)) {
                tr++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.finalSucces)) {
                su++;
            }
            if (ListeRequetesTech.get(i).getStatut().equals(Requete.Statut.finalAbandon)) {
                ab++;
            }
        }

        parStatut = ouvert + ouv + "\n"
                + enTraitement + tr + "\n"
                + succes + su + "\n"
                + abandon + ab + "\n";
        return parStatut;

    }
}